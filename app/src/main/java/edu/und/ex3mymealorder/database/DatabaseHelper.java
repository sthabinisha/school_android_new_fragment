package edu.und.ex3mymealorder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import edu.und.ex3mymealorder.adapter.ComboAdapter;

public class DatabaseHelper  extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    float price = 0;
    float price1 = 0;
    float lowPrice = 0;


    // Database Name
    private static final String DATABASE_NAME = "food_order.db";


    ArrayList<DatabaseInfo> comboList = new ArrayList<>();
    String combo_treat, comboNoComma, returnstringFood;
    float sum = 0;
    ComboAdapter adapter;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create food table
        db.execSQL(DatabaseInfo.CREATE_FOOD_TABLE);
        // create combo table
        db.execSQL(DatabaseInfo.CREATE_MEAL_TABLE);
        db.execSQL(DatabaseInfo.CREATE_FOOD_COMBO_TABLE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.FOOD_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.COMBO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.FOOD_COMBO);


        // Create tables again
        onCreate(db);
    }

    public boolean insertMenu(String foodname, String price) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.UNIQUE_NAME, foodname);
        values.put(DatabaseInfo.PRICE, price);


        // insert row
        db.insert(DatabaseInfo.FOOD_TABLE, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return true;
    }

    public void insertComboMeal(ArrayList<DatabaseInfo> foonameSelected, String price) {
        this.comboList = foonameSelected;
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbr = this.getReadableDatabase();


        ContentValues values = new ContentValues();
        StringBuilder sbString = new StringBuilder("");

        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        for (int i = 0; i < comboList.size(); i++) {
            sbString.append(foonameSelected.get(i).getUniqueName()).append(",");
        }
        combo_treat = sbString.toString();


        try {
            combo_treat = combo_treat.substring(0, combo_treat.length() - 1);
            String query = "select * from " + DatabaseInfo.COMBO_TABLE + " where " + DatabaseInfo.COMBO + " = '" + combo_treat + "'";
            Cursor cursor1 = dbr.rawQuery(query, null);
            if (cursor1.getCount() <= 0) {

                values.put(DatabaseInfo.COMBO, combo_treat);
                values.put(DatabaseInfo.PRICE, price);


                // insert row
                db.insert(DatabaseInfo.COMBO_TABLE, null, values);

            }
            String selectFromComboQuery = "SELECT " + DatabaseInfo.UNIQUE_ID + " FROM " + DatabaseInfo.COMBO_TABLE +
                    "  where " + DatabaseInfo.COMBO + "='" + combo_treat + "'";
            Cursor cursor = db.rawQuery(selectFromComboQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    int unique_id = cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID));
                    insertConTable(foonameSelected, unique_id);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // close db connection
        db.close();
        //insertConTable(id, foodname);


        // return newly inserted row id
        //return true;
    }


    public boolean insertConTable(ArrayList<DatabaseInfo> foonameSelected, int unique_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        this.comboList = foonameSelected;
        for (int i = 0; i < comboList.size(); i++) {
            values.put(DatabaseInfo.UNIQUE_ID, unique_id);
            values.put(DatabaseInfo.UNIQUE_NAME, foonameSelected.get(i).getUniqueName());
//            String selectFromFoodQuery = "SELECT "+ DatabaseInfo.UNIQUE_NAME +" FROM " + DatabaseInfo.FOOD_TABLE
//                    + " where " + DatabaseInfo.UNIQUE_NAME + "=" + foonameSelected.get(i).getUniqueName();
//            Cursor cursor = db.rawQuery(selectFromFoodQuery, null);
//            if(cursor.moveToFirst()){
//                do {
//                    values.put(DatabaseInfo.UNIQUE_ID, unique_id);
//                    values.put(DatabaseInfo.UNIQUE_NAME, cursor.getString(cursor.getColumnIndex(DatabaseInfo.UNIQUE_NAME)));
//                } while (cursor.moveToNext());
//
//            }

            // `id` and `timestamp` will be inserted automatically.
            // no need to add them
            db.insert(DatabaseInfo.FOOD_COMBO, null, values);


        }


        // insert row

        // close db connection
        db.close();


        // return newly inserted row id
        return true;
    }

    public List<DatabaseInfo> getMenu() {
        List<DatabaseInfo> menu = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.FOOD_TABLE + " ORDER BY " +
                DatabaseInfo.UNIQUE_NAME + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo info = new DatabaseInfo();
                info.setUnique_name(cursor.getString(cursor.getColumnIndex(DatabaseInfo.UNIQUE_NAME)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));

                menu.add(info);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();

        // return menu list
        return menu;
    }

    public List<DatabaseInfo> getCombo() {
        List<DatabaseInfo> menu = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE + " ORDER BY " +
                DatabaseInfo.UNIQUE_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo info = new DatabaseInfo();
                info.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID)));
                info.setCombo(cursor.getString(cursor.getColumnIndex(DatabaseInfo.COMBO)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));

                menu.add(info);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();

        // return menu list
        return menu;
    }

    public int UpdataPrice(Float update_price, String food_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put();
        return 1;
    }

    public boolean deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + DatabaseInfo.COMBO_TABLE);
        db.execSQL("delete from " + DatabaseInfo.FOOD_COMBO);
        db.execSQL("delete from " + DatabaseInfo.FOOD_TABLE);

        return true;

    }

    public boolean deleteCombo(String s) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseInfo.COMBO_TABLE, DatabaseInfo.COMBO + " = ?",
                new String[]{s});

        db.close();
        return true;

    }

    public int getComboCount() {
        String countQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int updateSelected(ArrayList<DatabaseInfo> databaseInfos) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int i = 0; i < databaseInfos.size(); i++) {
//            System.out.println( databaseInfos.get(i).getEditTextValue().toString());
            //values.put(DatabaseInfo.COMBO, comboList.get(i).getCOMBO());
            values.put(DatabaseInfo.PRICE, databaseInfos.get(i).getEditTextValue());
            db.update(DatabaseInfo.FOOD_TABLE, values, DatabaseInfo.UNIQUE_NAME + " = ?",
                    new String[]{String.valueOf(databaseInfos.get(i).getUniqueName())});


        }
        return 1;


    }

    public List<DatabaseInfo> getComboDetails(int ID) {
        List<DatabaseInfo> ComboDetails = new ArrayList<>();

        String selectFrombothQuery = "SELECT  distinct F.price, fc.food_combo  FROM " + DatabaseInfo.FOOD_TABLE +
                " F ," + DatabaseInfo.FOOD_COMBO + " fc," + DatabaseInfo.COMBO_TABLE + " c where  f.food_combo= fc.food_combo and fc.id=" + ID;
        System.out.println(selectFrombothQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectFrombothQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo comboDetailsData = new DatabaseInfo();
                comboDetailsData.setUnique_name(cursor.getString(cursor.getColumnIndex(comboDetailsData.UNIQUE_NAME)));
                comboDetailsData.setPrice(cursor.getFloat(cursor.getColumnIndex(comboDetailsData.PRICE)));
                ComboDetails.add(comboDetailsData);
            } while (cursor.moveToNext());

        }
        db.close();
        return ComboDetails;
    }

    String food_combo_treat;

    public Float getLowestPrice(ArrayList<DatabaseInfo> databaseInfos, ArrayList<String> food_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            food_combo_treat = String.join(",", food_name);
        }
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE +  " where " + DatabaseInfo.COMBO + " = '" + food_combo_treat+"'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo info = new DatabaseInfo();
                info.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID)));
                info.setCombo(cursor.getString(cursor.getColumnIndex(DatabaseInfo.COMBO)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));
                System.out.println(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));

            } while (cursor.moveToNext());
        }else {

            List<DatabaseInfo> ComboDetailstemp = getHighestPrice(databaseInfos);
            lowPrice = getLowPrice(ComboDetailstemp, food_name);

            if (lowPrice == 0) {

                for (int i = 0; i < databaseInfos.size(); i++) {
                    if (databaseInfos.get(i).getUniqueName() == ComboDetailstemp.get(0).getUniqueName()) {
                        databaseInfos.remove(i);
                        getLowestPrice(databaseInfos, food_name);

                    }
                }


            }
        }


        return null;
    }

    private float getLowPrice(List<DatabaseInfo> comboDetailstemp, ArrayList<String> food_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        List<DatabaseInfo> ComboDetailstemp = comboDetailstemp;
        ArrayList<String> compareName = new ArrayList<>();

        String comboID = "select * from " + DatabaseInfo.COMBO_TABLE + " where " + DatabaseInfo.COMBO + " like '%" + ComboDetailstemp.get(0).getUniqueName() + "%' order by length(" + DatabaseInfo.COMBO + ") desc";
        System.out.println(comboID);
        Cursor cursor = db.rawQuery(comboID, null);
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo comboDetailsData = new DatabaseInfo();
                String combo = cursor.getString(cursor.getColumnIndex(comboDetailsData.COMBO));
                int id = cursor.getInt(0);
                System.out.println(id);
                String getuniqueName = "select * from " + DatabaseInfo.FOOD_COMBO + " where " + DatabaseInfo.UNIQUE_ID + "= " + id;/*+ " and "+ DatabaseInfo.UNIQUE_NAME +" not like '%"+ ComboDetailstemp.get(0).getUniqueName() + "%'";
                    System.out.println(getuniqueName);*/
                Cursor cursor1 = db.rawQuery(getuniqueName, null);
                if (cursor1.moveToFirst()) {
                    do {
                        String uniqueName = cursor1.getString(0);

                        System.out.println(uniqueName);
                        compareName.add(uniqueName);


                    } while (cursor1.moveToNext());


                    // comboDetailsData.setPrice(cursor.getFloat(cursor.getColumnIndex(comboDetailsData.PRICE)));
//                ComboDetailstemp.add(comboDetailsData);
                }
                if (food_name.containsAll(compareName) && compareName.size() > 0) {
                    System.out.println("Common elements: " + compareName);

                    System.out.println("contains");
                    String combo_treat = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        combo_treat = String.join(",", compareName);
                    }
                    System.out.println(combo_treat);

                    String selectFromComboQuery = "SELECT " + DatabaseInfo.PRICE + ", " + DatabaseInfo.UNIQUE_ID + " FROM " + DatabaseInfo.COMBO_TABLE +
                            "  where " + DatabaseInfo.COMBO + "='" + combo_treat + "'";
                    Cursor cursor2 = db.rawQuery(selectFromComboQuery, null);
                    if (cursor2.moveToFirst()) {
                        do {
                            Float price2 = cursor2.getFloat(0);
                            int uid = cursor2.getInt(1);
                            if (combo_treat == food_combo_treat) {


                                price= price2;
                                System.out.println(price);
                            } else {
                                food_name.removeAll(compareName);



                                System.out.println(food_name);
                                System.out.println(food_name.size());
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                    returnstringFood = String.join(",", food_name);
                                }
                                for (int k = 0; k < food_name.size(); k++) {
                                    String selectPrice = "SELECT distinct " + DatabaseInfo.PRICE + " FROM " + DatabaseInfo.FOOD_TABLE +
                                            "  where " + DatabaseInfo.UNIQUE_NAME + "='" + food_name.get(k) + "'";
                                    System.out.println(selectPrice);

                                    Cursor cursor3 = db.rawQuery(selectPrice, null);
                                    if (cursor3.moveToFirst()) {
                                        do {
                                            price1 = cursor3.getFloat(0);
                                            System.out.println(price1);
                                            price1 = price1+price2;
                                        } while (cursor3.moveToNext());


                                       // price= price2+ price1;
                                        System.out.println(price1);

                                    }
                                }


                            }
                        } while (cursor2.moveToNext());

                    }
                }

                compareName.clear();
            }
            while (cursor.moveToNext());
        }
        return price;
    }

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list)
    {

        // Create a new LinkedHashSet
        Set<T> set = new LinkedHashSet<>();

        // Add the elements to set
        set.addAll(list);

        // Clear the list
        list.clear();

        // add the elements of set
        // with no duplicates to the list
        list.addAll(set);

        // return the list
        return list;
    }
    private List<DatabaseInfo> getHighestPrice(ArrayList<DatabaseInfo> databaseInfos) {
            List<DatabaseInfo> ComboDetailstemp = new ArrayList<>();

            String maxPrice = "select "+ DatabaseInfo.UNIQUE_NAME+ ", max(price) from "+ DatabaseInfo.FOOD_TABLE +" where ";

            for(int i = 0; i<databaseInfos.size(); i++){
                if (i == 0){ maxPrice+= DatabaseInfo.UNIQUE_NAME + " like '%" + databaseInfos.get(i).getUniqueName();}
                else{
                    //combo += sbString.append(databaseInfos.get(i).getUniqueName()).append(",");
                    maxPrice += "%' or "+ DatabaseInfo.UNIQUE_NAME + " like '%" + databaseInfos.get(i).getUniqueName();}

            } maxPrice += "%'";
            System.out.println( maxPrice  );
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(maxPrice, null);
            if(cursor.moveToFirst()){
                do {
                    DatabaseInfo comboDetailsData = new DatabaseInfo();
                    comboDetailsData.setUnique_name(cursor.getString(cursor.getColumnIndex(comboDetailsData.UNIQUE_NAME)));
                    comboDetailsData.setPrice(cursor.getFloat(1));
                    ComboDetailstemp.add(comboDetailsData);
                } while (cursor.moveToNext());

            }
            for(int i= 0; i<ComboDetailstemp.size(); i++){
                System.out.println(ComboDetailstemp.get(i).getUniqueName());
                System.out.println(ComboDetailstemp.get(i).getPRICE());

            }
            return ComboDetailstemp;

    }


}