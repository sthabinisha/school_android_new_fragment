package edu.und.ex3mymealorder.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.adapter.MealAdapter;
import edu.und.ex3mymealorder.database.DatabaseHelper;
import edu.und.ex3mymealorder.database.DatabaseInfo;


public class EnterDataFragment extends Fragment {
    Button enterData;
    EditText foodName;
    EditText foodPrice;
    private DatabaseHelper db;
    MealAdapter adapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enter_data, container, false);
        enterData = view.findViewById(R.id.enterMenu);
        foodName = view.findViewById(R.id.itemName);
        foodPrice = view.findViewById(R.id.itemPrice);
        final DatabaseInfo databaseInfo = new DatabaseInfo();
        db = new DatabaseHelper(getActivity());
        enterData.setOnClickListener(new View.OnClickListener() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }

            @Override
            public void onClick(View view) {
                if (foodName.getText().toString().isEmpty() || foodPrice.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Enter the Data", Toast.LENGTH_SHORT).show();

                }else{
                    DatabaseHelper helper = new DatabaseHelper(getActivity());
                    helper.insertMenu(foodName.getText().toString(), foodPrice.getText().toString());
                   // adapter.notifyDataSetChanged();


                    Toast.makeText(getActivity(), "Inserted", Toast.LENGTH_SHORT).show();




                }
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
