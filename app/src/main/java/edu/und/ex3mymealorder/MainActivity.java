package edu.und.ex3mymealorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import edu.und.ex3mymealorder.database.DatabaseHelper;
import edu.und.ex3mymealorder.fragment.AllComboFragment;
import edu.und.ex3mymealorder.fragment.AllFoodFragment;
import edu.und.ex3mymealorder.fragment.OrderFood;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView navigationView;
    private int mSelectedId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dl = (DrawerLayout)findViewById(R.id.activity_main);
        t = new ActionBarDrawerToggle(this, dl,R.string.Open, R.string.Close);

        dl.addDrawerListener(t);
        t.syncState();
        setFragment(new AllFoodFragment());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView = findViewById(R.id.nv);
        navigationView.setNavigationItemSelectedListener(this);




    }

    private void setFragment(Fragment fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_layout, fragment)
                .addToBackStack(null)
                .commit();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment= null;
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        int id = menuItem.getItemId();
        switch (menuItem.getItemId()) {
            case R.id.all_food:
                fragment = new AllFoodFragment();
                setFragment(fragment);
                break;

            case R.id.combo_food:
                        fragment = new AllComboFragment();
                        setFragment(fragment);
                break;

            case R.id.order_food:
                fragment = new OrderFood();
                setFragment(fragment);
                break;
            case R.id.delete:
                DatabaseHelper helper = new DatabaseHelper(this);
                helper.deleteAll();
                Toast.makeText(getApplicationContext(), "deleted", Toast.LENGTH_SHORT).show();
        }
        dl.closeDrawers();

        return true;
    }
}

